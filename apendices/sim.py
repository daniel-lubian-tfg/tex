def sistemaPlantaControlada(t,Yg,Yp,Ug,KP,KD,Yi,Ys,CONTROL="PD"):
    # inicializar los vectores
    Y  = numpy.zeros_like(Yg)
    U  = numpy.zeros_like(Ug)
    Up = numpy.zeros((Ug.shape[0],1))

    U[:,:] = Ug[:,:]
    Y[:,0] = Yg[:,0] + Yp[:,0]
    U[:,0] = Ug[:,0] + Up

    # crear integrador
    snl = scipy.integrate.ode(DerivadaSistemaNoLineal)
    snl.set_integrator('dopri5')
    snl.set_initial_value(Y[:,0],t[0])

    maxIter = t.shape[0]
    t_error = -1

    for i in range(1,maxIter):
        # control es guiado + perturbacion
        U[:,i] = Ug[:,i] + Up

        # integrar con Dormand-Prince
        snl.set_f_params(U[:,i])
        Y[:,i] = snl.integrate(t[i])
        if snl.successful() == False:
            print("Error: integracion PC: t=",t[i],", iter=",i)
            if t_error == -1: t_error = t[i]
            break
        snl.set_initial_value(snl.y + Yp[:,i],t[i])

        # derivadas para control PD
        Yd  = DerivadaSistemaNoLineal(t[i],Y[:,i],U[:,i])
        Ydg = DerivadaGuiadoVHL(t[i],Yg[:,i],Yi,Ys)

        # coger solo V y Theta
        X  =  Y2X( Y[:,i])
        Xg =  Y2X(Yg[:,i])
        Xd =  Y2X(Yd)
        Xdg = Y2X(Ydg)

        # realimentacion
        if CONTROL == "PD":
            Up = KP[:,:,i].dot(X-Xg) + KD[:,:,i].dot(Xd-Xdg)
        elif CONTROL == "P":
            Up = KP[:,:,i].dot(X-Xg)

    return(Y,U,t_error)
