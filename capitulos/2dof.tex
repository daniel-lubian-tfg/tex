% !TeX encoding = ISO-8859-1
% !TeX spellcheck = es_ES
% -!TeX root = ../main.tex

\chapter{Problema en dos grados de libertad}
\section{Introducci�n}
El objetivo de este cap�tulo es sentar las bases del problema y estudiarlo desde una aproximaci�n suficientemente buena, en la que no se pierda la f�sica del problema y en la que todav�a se pueda analizar de manera sencilla y directa los efectos m�s importantes.

Para ello, se va a analizar la din�mica de traslaci�n del veh�culo desde un punto del espacio a otro.

\section{Hip�tesis}
\label{ch:2dof:hipo}
\begin{itemize}
	\item Tierra plana.
	\item Sistema inercial.
	\item No hay viento.
	\item Masa puntual y constante.
	\item Cuerpo no sustentador, o sustentaci�n despreciable frente a la resistencia. (hip�tesis \ref{hip:nosustenta})
\end{itemize}

\section{Planteamiento de las ecuaciones del sistema}
Como ya se coment� en el cap�tulo \ref{ch:descripcion}, y de acuerdo con las hip�tesis planteadas en la secci�n \ref{ch:2dof:hipo}, en el sistema participan las siguientes fuerzas:
\begin{itemize}
	\item Peso del veh�culo lanzador, $W$
	\item Empuje proporcionado por el motor cohete, $T$
	\item Resistencia aerodin�mica, $D$
\end{itemize}

Adem�s, conviene recordar que el veh�culo se controla mediante control vectorial de empuje (TVC), por lo que aparecer� una magnitud angular denotada por $\delta$, la deflexi�n del empuje. Este caso, puesto que se considera el veh�culo reducido a un punto, se medir� desde la vertical, y no desde el eje longitudinal del veh�culo como ser�a natural.

Antes de plantear ninguna ecuaci�n, se establece un sistema de referencia basado en dos coordenadas cartesianas: $x$ (posici�n horizontal) y $h$ (altura), y centrado en un punto $O$, que se va a considerar inercial, dado que con los valores de altura y de tiempo que se van a tratar, la rotaci�n de la Tierra no juega un papel relevante (hip�tesis de Tierra plana). Este sistema se denotar� mediante $Oxh$.

Dado que �nicamente se est�n tomando dos grados de libertad en el plano, posici�n horizontal $x$ y altura $h$ sobre el nivel del suelo, no ser� necesario plantear las ecuaciones de momentos, bastar� con la segunda ley de Newton, o \emph{Ley Fundamental de la Din�mica}:
\begin{equation}
	\label{eq:Newton}
%	\sum \vect{F} = \frac{\diff (m\vect{v})}{\diff t}
	\sum \vect{F} = \der[(m\vect{v})]{t},
%	\sum \vect{f} = \der[(m\vect{v})]{t},
\end{equation}
que particularizada al conjunto de fuerzas descrito anteriormente es
\begin{equation} \label{eq:newton_particularizada}
	\vect W + \vect T + \vect D = m \der[\vect v]{t}.
%	\vect{f^g} + \vect{f^T} + \vect{f^a} = m \der[\vect v]{t}.
\end{equation}
%donde $m$ es la masa total del veh�culo.

A continuaci�n, se va a descomponer la ecuaci�n vectorial \ref{eq:newton_particularizada} en el plano seg�n dos sistemas de referencias distintos, para comprobar cu�l es el m�s adecuado para hacer el desarrollo.

\subsection{Sistema de referencia cartesiano}

\begin{figure}[tbh!]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/2dof-fuerzas-cartesianas.eps}
	\caption{Fuerzas sobre el veh�culo (sistema de referencia cartesiano).}
	\label{fig:2doffuerzascartesianas}
\end{figure}

La ecuaci�n~\ref{eq:newton_particularizada} se descompone en un sistema de referencia paralelo al sistema $Oxh$, pero centrado en el centro de masas del veh�culo, tal y como se muestra en la figura~\ref{fig:2doffuerzascartesianas}. As�, se obtiene
\begin{subequations}
\begin{align}
\vect{u}_x : \frac{W}{g} \frac{\diff^2 x}{\diff t^2} &= T \sin \delta + D \sin \theta\\
\vect{u}_h : \frac{W}{g} \frac{\diff^2 h}{\diff t^2} &= T \cos \delta + D \cos \theta - W,
\end{align}
\end{subequations}
teniendo en cuenta que $m = W/g$, siendo $g$ la gravedad y $\theta$ el �ngulo de asiento de la trayectoria.

Adem�s, se tiene que
\begin{subequations}
	\begin{align}
	\sin \theta &= \dot x \Big/ \sqrt{{\dot x}^2 + {\dot h}^2}\\
	\cos \theta &= \dot h \Big/ \sqrt{{\dot x}^2 + {\dot h}^2}\\
	V &= \sqrt{{\dot x}^2 + {\dot h}^2},
	\end{align}
\end{subequations}
por lo que se puede sustituir para conseguir
\begin{subequations}
	\begin{align}
	\frac{W}{g} \ddot x &= T \sin \delta + D \frac{\dot x }{\sqrt{{\dot x}^2 + {\dot h}^2}}\\
	\frac{W}{g} \ddot h &= T \cos \delta + D \frac{\dot h }{\sqrt{{\dot x}^2 + {\dot h}^2}} - W,
	\end{align}
\end{subequations}

T�picamente, se puede escribir $D = \frac{1}{2}\rho S_r V^2C_D$, donde $S_r$ ser�a la superficie de referencia y $C_D$ un coeficiente de resistencia adimensional que suele depender del n�mero de Mach, que a su vez depende de $V$ y de $h$. Entonces
\begin{subequations}\label{ec_2dof_cartesianas}
	\begin{align}
	\frac{W}{g(h)} \ddot x &= T \sin \delta + \frac{1}{2}\rho(h) S_r C_D(V,h) \dot x \sqrt{{\dot x}^2 + {\dot h}^2}\\
	\frac{W}{g(h)} \ddot h &= T \cos \delta + \frac{1}{2}\rho(h) S_r C_D(V,h) \dot h \sqrt{{\dot x}^2 + {\dot h}^2} - W,
	\end{align}
\end{subequations}
que es un sistema de dos ecuaciones diferenciales ordinarias de segundo orden no lineales y fuertemente acopladas. 

Sin embargo, se pueden simplificar si:
\begin{itemize}
	\item Se considera un intervalo de tiempo en el que las variaciones de $g(h)$ y de $\rho(h)$ no sean muy elevadas, por lo que esos t�rminos ser�n aproximadamente constantes
	\item La dependencia de $C_D$ con $V$ y $h$ no se tiene en cuenta (se asume un $C_D$ constante)
	\item Dado que $\vect{v} = -u\vect{u}_x-v\vect{u}_h$, se tiene  que $u = -\dot x$ y $v = -\dot h$
	\item Se define el par�metro $$C_B = \frac{\rho S_r C_D g}{2 W}$$ 
\end{itemize}

Aplicando esto, y reordenando t�rminos se llega a:
 
\begin{subequations}
	\begin{align}
	\dot u &= -C_B u^2 \sqrt{u^2 + v^2} - \frac{T}{W} \sin \delta \\
	\dot v &= -C_B uv \sqrt{u^2 + v^2} - \frac{T}{W} \cos \delta + g
	\end{align}
\end{subequations}

El sistema sigue siendo no lineal y acoplado, pero escrito de esta forma se podr�n intuir mejor algunas de las leyes de guiado que se propondr�n en la secci�n \ref{2dof-leyesGuiado}. Por supuesto, las variables de control de este conjunto de ecuaciones ser�an aquellas sobre las que se puede actuar m�s directamente desde el veh�culo, esto es, el empuje ($T$) y su deflexi�n ($\delta$).

\subsection{Sistema de referencia intr�nseco}

\begin{figure}[tbh!]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/2dof-fuerzas-intrinseco.eps}
	\caption{Fuerzas sobre el veh�culo (sistema de referencia intr�nseco a la trayectoria).}
	\label{fig:2doffuerzasintrinseco}
\end{figure}

De nuevo, la ecuaci�n~\ref{eq:newton_particularizada} se descompone en un sistema de referencia, pero esta vez ser� intr�nseco a la trayectoria. Estar� centrado en la masa puntual y estar� basado en los versores $\vect{t}$ y $\vect{n}$, los versores tangente y normal a la trayectoria, respectivamente. La configuraci�n de fuerzas en este sistema de referencia se muestra en la figura~\ref{fig:2doffuerzasintrinseco}.

\begin{subequations}
	\begin{align}
	\vect{t} : \frac{W}{g} \frac{\diff V}{\diff t} &= W\cos \theta - T \cos(\theta-\delta) - D\\
	\vect{n} : \frac{W}{g} V\frac{\diff\theta}{\diff t} &= W\sin \theta - T \sin(\theta-\delta),
	\end{align}
\end{subequations}
donde $\theta$ representa el �ngulo que forma el vector velocidad con la vertical. De nuevo el sistema de ecuaciones diferenciales es no lineal y est� acoplado, y las variables de control a seleccionar ser�an el empuje y su deflexi�n. Reordenando:

\begin{subequations}\label{eq_2dof_intrinsecas}
	\begin{align}
	\frac{\diff V}{\diff t} &= g\cos \theta - g\frac{T}{W} \cos(\theta-\delta) - \underbrace{\frac{g \rho C_D}{2 W}}_{C_B} V^2\\
	V\frac{\diff\theta}{\diff t} &= g\sin \theta - g\frac{T}{W} \sin(\theta-\delta),
	\end{align}
\end{subequations}

Este sistema tiene algunas ventajas frente a \ref{ec_2dof_cartesianas}, y es que es m�s intuitivo ver la f�sica de este problema en t�rminos de aceleraci�n tangencial y normal, adem�s de que la linealizaci�n es m�s sencilla.

\section{Linealizaci�n de las ecuaciones}

En esta secci�n se va a obtener el sistema de ecuaciones linealizado seg�n el \emph{m�todo de las peque�as perturbaciones} para \ref{eq_2dof_intrinsecas}. Para ello, se linealiza alrededor de los valores de guiado, que se denotar�n con el sub�ndice $\square_g$. Hay que tener en cuenta que se asume un valor constante de $C_B$, $g$ y $W$ para la linealizaci�n. Por tanto, esto implica que
\begin{subequations}\label{perturbaciones}
	\begin{align}
		V &= V_g + \Delta V + \mathcal{O}(\Delta^2)\\
		\theta &= \theta_g + \Delta \theta+ \mathcal{O}(\Delta^2)\\
%		h &= h_g + \Delta h + \mathcal{O}(\Delta^2)\\
%		x &= x_g + \Delta x + \mathcal{O}(\Delta^2)\\
		T &= T_g + \Delta T + \mathcal{O}(\Delta^2)\\
		\delta &= \delta_g + \Delta\delta + \mathcal{O}(\Delta^2)
	\end{align}
\end{subequations}

Por supuesto, las leyes de guiado (secci�n \ref{2dof-leyesGuiado}) deber�n cumplir \ref{eq_2dof_intrinsecas}, por lo que
\begin{subequations}\label{eq_2dof_intrinsecas_g}
	\begin{align}
		\frac{\diff V_g}{\diff t} &= g\cos \theta_g - g\frac{T_g}{W} \cos(\theta_g-\delta_g) - C_B V_g^2\\
		V_g\frac{\diff\theta_g}{\diff t} &= g\sin \theta_g - g\frac{T_g}{W} \sin(\theta_g-\delta_g).
	\end{align}
\end{subequations}

Si se sustituye \ref{perturbaciones} en \ref{eq_2dof_intrinsecas} y se aplican las f�rmulas del coseno y del seno de la suma de dos �ngulos\footnote{$\cos(a+b) = \cos a \cos b - \sin a \sin b$; $\sin(a+b) = \sin a \cos b + \cos a \sin b$} se obtiene para la ecuaci�n en $\diff V/\diff t$:
\begin{equation*}
\begin{aligned}
		\frac{\diff V_g}{\diff t} + \frac{\diff \Delta V}{\diff t} &={} g\big(\cos \theta_g\cos\Delta\theta-\sin\theta_g\sin\Delta\theta\big)-\\&- g\Bigg(\frac{T_g}{W}+\frac{\Delta T}{W}\Bigg)\big( \cos(\theta_g-\delta_g)\cos(\Delta\theta-\Delta\delta)-\sin(\theta_g-\delta_g)\sin(\Delta\theta-\Delta\delta)\big)-\\&- C_B\big(V_g^2 + 2V_g\Delta V + (\Delta V)^2\big) + \mathcal{O}(\Delta^2),
\end{aligned}
\end{equation*}
y para la ecuaci�n en $V\diff \theta/\diff t$:
\begin{equation*}
	\begin{aligned}
		(V_g+\Delta V)\Bigg(\frac{\diff\theta_g}{\diff t} +\frac{\diff\Delta\theta}{\diff t} \Bigg) &={} g\big(\sin \theta_g\cos\Delta\theta+\sin\Delta\theta\cos\theta_g \big) -\\
		&- g\Bigg(\frac{T_g}{W}+\frac{\Delta T}{W}\Bigg)\bigg( \cos(\theta_g-\delta_g)\sin(\Delta\theta-\Delta\delta)+\\
		&+\sin(\theta_g-\delta_g)\cos(\Delta\theta-\Delta\delta)\bigg) + \mathcal{O}(\Delta^2)
	\end{aligned}
\end{equation*}

Si ahora se desprecian los t�rminos de segundo orden o superior, se asume que $\Delta\theta$ y $\Delta\delta$ son �ngulos peque�os y por tanto se pueden sustituir los senos y los cosenos por sus desarrollos de Taylor y se tienen en cuenta las igualdades \ref{eq_2dof_intrinsecas_g}, se llega al siguiente sistema:
\begin{equation*}
	\begin{aligned}
	\frac{\diff\Delta V}{\diff t} &={} -2 C_B V_g \Delta V - g \sin \theta_g \Delta \theta	+ g\frac{T_g}{W}\sin(\theta_g-\delta_g)\Delta\theta-\\&- g\frac{1}{W}\cos(\theta_g-\delta_g)\Delta T - g\frac{T_g}{W}\sin(\theta_g-\delta_g)\Delta\delta
	\end{aligned}
\end{equation*}
\begin{equation*}
	\begin{aligned}
		V_g\frac{\diff\Delta\theta}{\diff t} &={} -\frac{\diff\theta_g}{\diff t} \Delta V + g\cos\theta_g \Delta \theta	- g\frac{T_g}{W}\cos(\theta_g-\delta_g)\Delta\theta-\\&- g\frac{1}{W}\sin(\theta_g-\delta_g)\Delta T + g\frac{T_g}{W}\cos(\theta_g-\delta_g)\Delta\delta,
	\end{aligned}
\end{equation*}
que se puede escribir matricialmente de la forma
\begin{equation}\label{eq:2dof_mat}
\begin{aligned}
	\frac{\diff}{\diff t}
	\begin{bmatrix}
		\Delta V \\
		\Delta \theta
	\end{bmatrix}
	&=
	\underbrace{\begin{bmatrix}
		-2 C_B V_g & g\big(\frac{T_g}{W}\sin(\theta_g-\delta_g)-\sin\theta_g  \big)\\
		-\frac{1}{V_g}\frac{\diff \theta_g}{\diff t} & \frac{g}{V_g}\big(\cos\theta_g -\frac{T_g}{W}\cos(\theta_g-\delta_g)\big) 
	\end{bmatrix}}_{\mat A}
	\begin{bmatrix}
		\Delta V \\
		\Delta \theta
	\end{bmatrix}+\\
&+
	\underbrace{\begin{bmatrix}
		-\frac{g}{W}\cos(\theta_g-\delta_g) & -\frac{g T_g}{W}\sin(\theta_g-\delta_g) \\
		-\frac{g}{V_g W}\sin(\theta_g-\delta_g) & +\frac{g T_g}{V_g W}\cos(\theta_g-\delta_g) \\
	\end{bmatrix}}_{\mat B}
	\begin{bmatrix}
		\Delta T \\
		\Delta \delta
	\end{bmatrix}
\end{aligned}
\end{equation}

Esta representaci�n matricial es el llamado modelo de \emph{espacio de estados}. $\mat A$ y $\mat B$ son las matrices de coeficientes del sistema linealizado y, en principio, no son de coeficientes constantes debido a la presencia de variables de guiado ($V_g$, $\theta_g$, $T_g$ y $\delta_g$), principalmente. Las leyes de guiado (secci�n \ref{2dof-leyesGuiado}) son las que dar�n la evoluci�n de estos coeficientes.

Seguidamente, se pueden definir $\vect x = [V, \theta]^T$ y $\vect u = [ T, \delta]^T$ como los vectores de estado y de control. As�, se puede escribir la ecuaci�n~\ref{eq:2dof_mat} de forma mucho m�s compacta:
\begin{equation}
	\Delta\vect{\dot x} = \mat{A}\Delta\vect{x} + \mat{B}\Delta\vect{u}
\end{equation}

Por �ltimo, parece interesante escribir las matrices $\mat A$ y $\mat B$ mediante otras expresiones de los coeficientes (usando las expresiones \eqref{eq_2dof_intrinsecas_g}) para poder ver mejor el valor de esos t�rminos.

\begin{equation}\label{eq:A}
%	\begin{aligned}
%		\mat A & = 
		\mat A = 
		\begin{bmatrix}
			-2 C_B V_g & g\big(\frac{T_g}{W}\sin(\theta_g-\delta_g)-\sin\theta_g  \big)\\
			-\frac{1}{V_g}\frac{\diff \theta_g}{\diff t} & \frac{g}{V_g}\big(\cos\theta_g -\frac{T_g}{W}\cos(\theta_g-\delta_g)\big) 
		\end{bmatrix}
%		\\& = 
		= 		
		\begin{bmatrix}
		-2 C_B V_g & -V_g\frac{\diff \theta_g}{\diff t}\\
		-\frac{1}{V_g}\frac{\diff \theta_g}{\diff t} & \frac{\diff V_g}{\diff t} + C_B V_g^2
		\end{bmatrix}
%	\end{aligned}
\end{equation}

Un aspecto interesante que se puede observar en la matriz $\mat A$ es que en los t�rminos de la diagonal secundaria son directamente proporcionales a $\dot\theta_g$. Si por ejemplo se establece que el valor en la ley de guiado sea nulo, es decir, la velocidad apunte siempre en la misma direcci�n, la matriz $\mat A$ ser� diagonal. Se puede destacar tambi�n que el t�rmino $A_{11}$ es negativo durante toda la trayectoria si la velocidad es positiva, esto es, si el veh�culo avanza hacia la superficie terrestre (seg�n se ha definido anteriormente). En cuanto al t�rmino $A_{22}$, $C_B V_g^2$ es positivo y $\dot V_g$ deber�a ser negativo, por lo que habr� que ver si se compensan el uno con el otro y en qu� momento lo hacen, pues esa anulaci�n podr�a dar lugar a inestabilidades de car�cter matem�tico si adem�s $\dot\theta_g$ es nulo.

\begin{equation}\label{eq:B}
	\begin{aligned}
	\mat B & = 
%	\mat B = 
	\begin{bmatrix}
		-\frac{g}{W}\cos(\theta_g-\delta_g) & -\frac{g T_g}{W}\sin(\theta_g-\delta_g) \\
		-\frac{g}{V_g W}\sin(\theta_g-\delta_g) & +\frac{g T_g}{V_g W}\cos(\theta_g-\delta_g) \\
	\end{bmatrix}
	\\& = 
%	= 		
	\begin{bmatrix}
		\frac{1}{T_g}(\frac{\diff V_g}{\diff t} - g\cos\theta_g + C_B V_g^2) & V_g\frac{\diff \theta_g}{\diff t}-g\sin\theta_g\\
		 \frac{1}{T_g}\frac{\diff \theta_g}{\diff t}-\frac{g}{V_g T_g}\sin\theta_g & -\frac{1}{V_g}(\frac{\diff V_g}{\diff t} - g\cos\theta_g + C_B V_g^2)
	\end{bmatrix}
	\end{aligned}
\end{equation}

Tras mirar los t�rminos de $\mat B$, uno se da cuenta r�pidamente que los t�rminos de la primera columna deber�n ser menores en orden de magnitud que los de la segunda columna, ya que esta �ltima est� multiplicada por el empuje. Si se quisiera que la matriz sea diagonal dominante, la diferencia entre $\theta_g$ y $\delta_g$ no deber�a ser muy grande para que los t�rminos en seno sean m�s peque�os o, visto en la segunda expresi�n de $\mat B$, si $\dot\theta_g$ y $\theta_g$ son muy peque�os o nulos (descenso pr�cticamente vertical y rectil�neo). Que la matriz $\mat B$ sea diagonal dominante implicar�a que el control en empuje afectar� principalmente al m�dulo de la velocidad y que la orientaci�n del empuje afectar� principalmente a la direcci�n de la velocidad, es decir, que las din�micas estar�an m�s desacopladas.

\section{Leyes de guiado propuestas}\label{2dof-leyesGuiado}

Es importante destacar el hecho de que en el problema planteado en este cap�tulo hay dos grados de libertad. Esto implica que se deben introducir dos restricciones para cerrar el problema. Estas restricciones son las leyes de guiado y han de imponerse para que la soluci�n est� completamente definida.

Sin embargo, estas leyes de guiado no pueden ser cualesquiera. Es necesario seleccionar aquellas que sean adecuadas y factibles para conseguir el comportamiento del sistema deseado, sin perder la realidad f�sica, y teniendo en mente las capacidades del sistema que se est� estudiando.

\subsection{Ley de variaci�n de la velocidad con la altura lineal y de �ngulo de asiento de la trayectoria constante}



\subsection{Ley de empuje constante y de �ngulo de asiento de la trayectoria constante}



\section{Estudio del sistema en planta libre}

\section{Sistema de control planteado}

Dado que la planta libre no es capaz de reducir y eliminar las perturbaciones, se hace necesario establecer un sistema de control. Se plantea la siguiente realimentaci�n:
\begin{equation}
	\Delta \vect{u} = \mat{K^P}\Delta \vect{x} + \mat{K^D}\Delta \vect{\dot x}
\end{equation}



\cite{RobustPP}


\section{Estudio del sistema en planta controlada}
